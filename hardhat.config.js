require("@nomiclabs/hardhat-waffle");
require("dotenv").config();

const rinkebyUrl = process.env.ALCHEMY_RINKEBY_URL
  ? process.env.ALCHEMY_RINKEBY_URL
  : "";

console.log(rinkebyUrl);

module.exports = {
  solidity: "0.8.3",
  networks: {
    localhost: {
      url: "http://localhost:8545",
      //gasPrice: 125000000000,//you can adjust gasPrice locally to see how much it will cost on production
      /*
			  notice no mnemonic here? it will just use account 0 of the hardhat node to deploy
			  (you can put in a mnemonic here to set the deployer locally)
			*/
    },
    rinkeby: {
      url: rinkebyUrl,
      accounts:
        process.env.ACCOUNT_KEY !== undefined ? [process.env.ACCOUNT_KEY] : [],
    },
    // etherscan: {
    //   // Your API key for Etherscan
    //   // Obtain one at https://etherscan.io/
    //   apiKey: process.env.ETHER_API,
    // },
  },
};
