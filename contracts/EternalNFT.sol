//SPDX-License-Identifier: MIT
pragma solidity 0.8.3;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import { Base64 } from "./libraries/Base64.sol";


contract EternalNFT is  ERC721Enumerable, ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenId;
   
    string public collectionName;
    string public collectionSymbol;

    mapping(address => uint256[]) public userOwnedTokens;
    mapping(uint256 => uint256) public tokenIsAtIndex;

     function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

     function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    string baseSvg = "<svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMinYMin meet' viewBox='0 0 350 350'><style>.base { fill: white; font-family: serif; font-size: 24px; }</style><rect width='100%' height='100%' fill='black' /><text x='50%' y='50%' class='base' dominant-baseline='middle' text-anchor='middle'>";

    string[] element = [
        'Fire',
        'Wind',
        'Wave',
        'Earth',
        'Thunder',
        'Space',
        'Time'
    ];

    string[] weapon = [
        'Sword',
        'Spear',
        'Shield',
        'Hammer',
        'Saber',
        'Axe',
        'Bow'
    ];

    string[] rank = [
        'Lord',
        'King',
        'Emperor',
        'Venerable',
        'Ancestor',
        'Saint',
        'God'
    ];

    constructor() ERC721("EternalNFT", "ENFT") {
        collectionName = name();
        collectionSymbol = symbol();
    }


    function random(string memory _input) internal pure returns(uint256) {
        return uint256(keccak256(abi.encodePacked(_input)));
    }


    function pickFirstWord(uint256 tokenId) public view returns(string memory) {
        uint256 rand = random(string(abi.encodePacked("element", Strings.toString(tokenId))));
        rand = rand % element.length;
        return element[rand];
    }


    function pickSecondWord(uint256 tokenId) public view returns(string memory) {
        uint256 rand = random(string(abi.encodePacked("weapon", Strings.toString(tokenId))));
        rand = rand % weapon.length;
        return weapon[rand];
    }

    function pickThirdWord(uint256 tokenId) public view returns(string memory) {
        uint256 rand = random(string(abi.encodePacked("rank", Strings.toString(tokenId))));
        rand = rand % rank.length;
        return rank[rand];
    }
    function show() external view returns(string memory) {
      
        return "asd";
    }
    // function showS() public view returns(uint256[] calldata) {
      
    //     return userOwnedTokens;
    // }

    //external data MINT

    function createEternalNFT() public returns(uint256) {
        uint256 newItemId = _tokenId.current();

        string memory first = pickFirstWord(newItemId);
        string memory second = pickSecondWord(newItemId);
        string memory third = pickThirdWord(newItemId);
        string memory combinedWord = string(abi.encodePacked(first,second,third));

        string memory finalSvg = string(abi.encodePacked(baseSvg, first, second, third, "</text></svg>"));

        string memory json = Base64.encode(
            bytes(
                string(
                    abi.encodePacked(
                    '{"name": "',
                        combinedWord,
                        '", "description": "A highly acclaimed collection Eternal Warriors", "image": "data:image/svg+xml;base64,',
                        Base64.encode(bytes(finalSvg)),
                    '"}'
                    )
                )
            )
        );

//dibuat URI nya
        string memory finalTokenURI = string(abi.encodePacked(
            "data:application/json;base64,", json
        ));
    
        _safeMint(msg.sender, newItemId);

        //finaltokenUri harusnya meta IPFS, jadi sblum ini hrus upload dulu di SC
        // or I can passvalue JSON as string(gk bisa pass karena mau masukin ditengah gmna)


        _setTokenURI(newItemId, finalTokenURI);
        userOwnedTokens[msg.sender].push(newItemId);

        uint256 arrayLength = userOwnedTokens[msg.sender].length;
        tokenIsAtIndex[newItemId] = arrayLength;
        // tokenOfOwnerByIndex(msg.sender, newItemId);

        _tokenId.increment();

        return newItemId;
    }

    // transfer(from, to, tokenId) {
    //     // Transfer logic
    //     uint256 tokenIndex = tokenIsAtIndex[tokenId];
    //     userOwnedTokens[from][tokenIndex] = 000; // TO DENOTE THAT THE TOKEN HAS BEEN TRANSFERRED, YOU CAN USE ANY OTHER NUMBER
    // }
    // Next time when userOwnedTokens is called you can keep a check wherever the tokenId is 000, those tokens have already been transferred and you can decide not to show it to the user on the frontend.

}
//poly meta
//double contract load


// meta, can u update meta?
//1155
// trf nft
//update contract?
//auction?
//royalty fee






    // do I need to save with address?
    //possible save in mapping again, but can i get from mintContract? how do i get mint nft address in web3


// function tokensOfOwner(address _owner) external view returns(uint256[] ownerTokens) {
//     uint256 tokenCount = balanceOf(_owner);

//     if (tokenCount == 0) {
//         // Return an empty array
//         return new uint256[](0);
//     } else {
//         uint256[] memory result = new uint256[](tokenCount);
//         uint256 totalCats = totalSupply();
//         uint256 resultIndex = 0;

//         // We count on the fact that all cats have IDs starting at 1 and increasing
//         // sequentially up to the totalCat count.
//         uint256 catId;

//         for (catId = 1; catId <= totalCats; catId++) {
//             if (kittyIndexToOwner[catId] == _owner) {
//                 result[resultIndex] = catId;
//                 resultIndex++;
//             }
//         }

//         return result;
//     }
// }


//use 
//https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v4.1.0/contracts/token/ERC721/extensions/ERC721Enumerable.sol